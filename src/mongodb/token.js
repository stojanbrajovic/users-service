import getDb from "./getDb";

const COLLECTION = "token";

const getCollection = async () => {
	const db = await getDb();
	return db.collection(COLLECTION);
};

export const insertToken = async ({ token, userId }) => {
	const col = await getCollection();
	await col.insertOne({ token, userId });
};

export const findToken = async token => {
	const col = await getCollection();
	return col.findOne({ token });
};

export const deleteToken = async token => {
	const col = await getCollection();
	return col.deleteOne({ token });
};
