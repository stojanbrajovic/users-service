import getDb from "./getDb";

const USERS_COLLECTION = "users";

const defaultUser = {
	_id: "123",
	username: "Username",
	email: "testuser@test.com",
	validated: false,
	passwordHash: "hash",
	salt: "salt",
};

const USER_UNIQUE_FIELDS = ["email", "username"];

const initCollectionUniqueFields = async () => {
	const db = await getDb();
	const collection = db.collection(USERS_COLLECTION);
	await collection.dropIndexes();
	const promises = USER_UNIQUE_FIELDS.map(field =>
		collection.createIndex(
			{ [field]: 1 },
			{
				name: field,
				unique: true,
				collation: { locale: "en", strength: 1 },
				partialFilterExpression: { [field]: { $exists: true } },
			},
		),
	);
	await Promise.all(promises);
};
const initPromise = initCollectionUniqueFields();

const getCollection = async () => {
	await initPromise;
	const db = await getDb();
	const collection = db.collection(USERS_COLLECTION);
	return collection;
};

const findUser = async (...queries) => {
	const col = await getCollection();
	return col.findOne({ $or: queries });
};

const insertUser = async user => {
	const col = await getCollection();
	return col.insertOne(user);
};

const updateUser = async (_id, changeData, upsert = false) => {
	const col = await getCollection();

	const updateQuery = {};
	Object.keys(changeData).forEach(key => {
		const value = changeData[key];
		if (value) {
			updateQuery.$set = { ...updateQuery.$set, [key]: value };
		} else {
			updateQuery.$unset = { ...updateQuery.$unset, [key]: "" };
		}
	});

	await col.updateOne({ _id }, updateQuery, { upsert });
};

export { findUser, defaultUser, insertUser, updateUser };
