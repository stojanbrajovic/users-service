// const nodemailer = require("nodemailer");
// const config = require("../../config.json");
// const { VERIFICATION_URL } = require("./routes/constants");

// const transporter = nodemailer.createTransport({
// 	service: "gmail",
// 	auth: {
// 		user: config.email,
// 		pass: config.password,
// 	},
// });

// const createEmailText = verificationId => {
// 	return (
// 		"Click on this link to verify your registration\n" +
// 		config.rootUrl +
// 		VERIFICATION_URL +
// 		"?_id=" +
// 		verificationId
// 	);
// };

// const sendEmail = async (verificationId, userEmail) =>
// 	new Promise((resolve, reject) => {
// 		const mailOptions = {
// 			from: "Calories Counter <email@address.com>",
// 			to: userEmail,
// 			subject: "Account verification",
// 			text: createEmailText(verificationId),
// 		};

// 		transporter.sendMail(mailOptions, error => {
// 			if (error) {
// 				reject(error);
// 			} else {
// 				resolve();
// 			}
// 		});
// 	});

// module.exports = sendEmail;
