import logger from "../logger";

const createHandler = (handler, forcedErrorMessage = null) => async (
	req,
	res,
) => {
	const requestInfo = {
		path: req.route.path,
		body: JSON.stringify(req.body),
		query: JSON.stringify(req.query),
		headers: JSON.stringify(req.headers),
	};

	logger.info("Received request", requestInfo);

	const send = result => {
		res.send(result);

		logger.info("Response sent", {
			...requestInfo,
			result,
		});
	};

	try {
		await handler(req, { send });
	} catch (error) {
		res.status(500).send({
			error: {
				message: forcedErrorMessage || error.message,
			},
		});

		logger.error(error, requestInfo);
	}
};

export default createHandler;
