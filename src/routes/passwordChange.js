import passwordChangeService from "../services/passwordChange";
import { validatePassword } from "../services/password";
import createHandler from "./createHandler";

const passwordChange = async ({ body, headers }, res) => {
	const { oldPassword, newPassword } = body;
	const { token } = headers;

	validatePassword(oldPassword);
	validatePassword(newPassword);
	if (!token) {
		throw new Error("No token provided");
	}

	await passwordChangeService(oldPassword, newPassword, token);

	res.send({ success: true });
};

export default createHandler(passwordChange);
