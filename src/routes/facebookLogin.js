import fetch from "node-fetch";
import externalServiceLogin from "../services/externalServiceLogin";
import createHandler from "./createHandler";

const getFacebookGraphUrl = (id, token) =>
	`https://graph.facebook.com/${id}?fields=email&access_token=${token}"`;

const facebookLogin = async ({ body }, res) => {
	const { fbId, token: facebookToken } = body;

	if (!fbId || !facebookToken) {
		throw new Error("No needed params");
	}

	const userVerificationResponse = await fetch(
		getFacebookGraphUrl(fbId, facebookToken),
	);
	const { error, email } = await userVerificationResponse.json();
	if (error || !email) {
		throw new Error("Facebook Graph auth failed");
	}

	const result = await externalServiceLogin(email);
	res.send(result);
};

export default createHandler(facebookLogin);
