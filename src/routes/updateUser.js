import { validateEmail } from "./validations";
import updateUserService from "../services/updateUser";
import createHandler from "./createHandler";

const updateUser = async (req, res) => {
	const { email, username } = req.body;

	if (!email && !username) {
		throw new Error("Nothing to update");
	}

	email && validateEmail(email);

	const updateData = { email, username };

	const { token } = req.headers;

	if (!token) {
		throw new Error("Token not provided");
	}

	await updateUserService(updateData, token);

	res.send({ success: true });
};

export default createHandler(updateUser, "Update failed");
