const EMAIL_REGEXP = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
const MIN_PASSWORD_LENGTH = 8;

export const validateEmail = email => {
	if (!EMAIL_REGEXP.test(email)) {
		throw new Error("Invalid email");
	}
};

export const validatePassword = pass => {
	if (pass.length < MIN_PASSWORD_LENGTH) {
		throw new Error(
			`Password must be at least ${MIN_PASSWORD_LENGTH} characters long`,
		);
	}
};
