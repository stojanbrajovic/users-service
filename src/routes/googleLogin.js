import fetch from "node-fetch";
import createHandler from "./createHandler";
import externalServiceLogin from "../services/externalServiceLogin";

const GOOGLE_OAUTH_URL = "https://oauth2.googleapis.com/tokeninfo?id_token=";

const googleLogin = async ({ body }, res) => {
	const { tokenId } = body;
	const response = await fetch(`${GOOGLE_OAUTH_URL}${tokenId}`);
	const { email, error } = await response.json();
	if (error || !email) {
		throw new Error("Google login failed");
	}

	const result = await externalServiceLogin(email);
	res.send(result);
};

export default createHandler(googleLogin);
