import createUserService from "../services/createUser";
import createHandler from "./createHandler";
import { validateEmail, validatePassword } from "./validations";

const createUser = async (req, res) => {
	const { email, password, username } = req.body;
	if (!email && !username) {
		throw new Error("Must provide either email or username");
	}

	email && validateEmail(email);
	validatePassword(password);
	await createUserService(username, password, email);

	await res.send({ success: true });
};

export default createHandler(createUser, "Create user failed");
