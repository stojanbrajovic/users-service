import { validateEmail, validatePassword } from "./validations";
import createHandler from "./createHandler";
import loginService from "../services/login";

const login = async (req, res) => {
	const { username, email, password } = req.body;

	if (!password) {
		throw new Error("must send password");
	}
	const numberOfIdentiers = [username, email].reduce(
		(res, param) => (param ? res + 1 : res),
		0,
	);
	if (numberOfIdentiers === 0) {
		throw new Error("must provide email, username or phone number");
	}
	if (numberOfIdentiers > 1) {
		throw new Error("must provide only one of email, username or phone number");
	}

	validatePassword(password);
	email && validateEmail(email);

	const response = await loginService({ username, email }, password);

	res.send(response);
};

export default createHandler(login, "Login failed");
