import tokenLoginService from "../services/tokenLogin";
import createHandler from "./createHandler";

const tokenLogin = async ({ headers }, res) => {
	const { token } = headers;

	if (!token) {
		throw new Error("must provide token");
	}

	const result = await tokenLoginService(token);

	res.send(result);
};

export default createHandler(tokenLogin, "Token login failed");
