import express from "express";
import { port } from "../config.json";
import createUser from "./routes/createUser";
import login from "./routes/login";
import tokenLogin from "./routes/tokenLogin.js";
import updateUser from "./routes/updateUser.js";
import passwordChange from "./routes/passwordChange.js";
import facebookLogin from "./routes/facebookLogin.js";
import googleLogin from "./routes/googleLogin.js";

// Parse JSON bodies (as sent by API clients)
const app = express();
app.use(express.json());

app.post("/user", createUser);
app.patch("/user", updateUser);

app.post("/login", login);
app.post("/tokenLogin", tokenLogin);
app.post("/facebookLogin", facebookLogin);
app.post("/googleLogin", googleLogin);

app.post("/passwordChange", passwordChange);

app.listen(port, () => console.log(`App listening on port ${port}!`));
