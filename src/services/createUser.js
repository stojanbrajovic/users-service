import { hashPassword, getRandomString } from "./password";
import { insertUser } from "../mongodb/users";

const createUser = async (username, password, email) => {
	const salt = getRandomString();
	const passwordHash = hashPassword(password, salt);
	const user = {
		passwordHash,
		salt,
	};
	if (username) {
		user.username = username;
	}
	if (email) {
		user.email = email;
	}

	return insertUser(user);
};

export default createUser;
