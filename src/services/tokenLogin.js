import {
	tokenShouldBeRenewed,
	getUniqueToken,
	getUserFromToken,
} from "./token";
import { insertToken } from "../mongodb/token";
import getUserData from "./getUserData";

const tokenLogin = async tokenArg => {
	let token = tokenArg;
	const user = await getUserFromToken(token);
	if (tokenShouldBeRenewed(token)) {
		token = await getUniqueToken(user._id);
		await insertToken({ token, userId: user._id });
	}
	return { user: getUserData(user), token };
};

export default tokenLogin;
