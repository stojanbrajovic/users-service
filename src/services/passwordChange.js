import { getUserFromToken } from "./token";
import { validatePassword, hashPassword } from "./password";
import { updateUser } from "../mongodb/users";

const changePassword = async (oldPassword, newPassword, token) => {
	const user = getUserFromToken(token);
	const { salt, passwordHash, _id } = user;

	const isOldPasswordValid = validatePassword(oldPassword, passwordHash, salt);
	if (!isOldPasswordValid) {
		throw new Error("Old password not valid");
	}

	const newPasswordHash = hashPassword(newPassword, salt);

	await updateUser(_id, { passwordHash: newPasswordHash });
};

export default changePassword;
