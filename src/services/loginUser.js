import { getUniqueToken } from "./token";
import { insertToken } from "../mongodb/token";
import getUserData from "./getUserData";

const loginUser = async user => {
	const { _id } = user;
	const token = getUniqueToken(_id);
	await insertToken({ token, userId: _id });
	return { user: getUserData(user), token };
};

export default loginUser;
