import { getUserFromToken } from "./token";
import { updateUser as dbUpdateUser } from "../mongodb/users";

const updateUser = async (userUpdate, token) => {
	const user = await getUserFromToken(token);
	await dbUpdateUser(user._id, userUpdate);
};

export default updateUser;
