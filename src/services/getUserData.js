const getUserData = ({ username, email, _id }) => ({
	_id,
	username,
	email,
});

export default getUserData;
