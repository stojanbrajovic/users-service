import { randomBytes, createHmac } from "crypto";

export const getRandomString = (length = 16) => {
	return randomBytes(Math.ceil(length / 2))
		.toString("hex") /** convert to hexadecimal format */
		.slice(0, length); /** return required number of characters */
};

export const hashPassword = (password, salt) => {
	const hash = createHmac("sha512", salt); /** Hashing algorithm sha512 */
	hash.update(password);
	const value = hash.digest("hex");
	return value;
};

export const validatePassword = (password, hash, salt) => {
	const result = hashPassword(password, salt);
	return result === hash;
};
