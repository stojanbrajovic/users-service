import { findUser } from "../mongodb/users";
import { validatePassword } from "./password";
import { requireVerificationForLogin } from "../../config.json";
import loginUser from "./loginUser";

const cleanUserIdentifier = userQuery => {
	const query = { ...userQuery };
	Object.keys(query).forEach(k => {
		const val = query[k];
		if (!val) {
			delete query[k];
		}
	});

	return query;
};

const login = async (userIdentifier, password) => {
	const userQuery = cleanUserIdentifier(userIdentifier);
	const user = await findUser(userQuery);
	if (!user) {
		throw new Error("no user found");
	}

	const { passwordHash, salt, validated } = user;
	if (requireVerificationForLogin && !validated) {
		throw new Error("user not validated");
	}

	const isValid = validatePassword(password, passwordHash, salt);

	if (!isValid) {
		throw new Error("invalid password");
	}

	return loginUser(user);
};

export default login;
