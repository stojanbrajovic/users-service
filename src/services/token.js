import jwt from "jsonwebtoken";
import { findToken, deleteToken } from "../mongodb/token";
import { tokenPrivateKey } from "../../config.json";
import { findUser } from "../mongodb/users";

const DAY_IN_SECONDS = 24 * 60 * 60;
const TOKEN_VALID_SECONDS = 10 * DAY_IN_SECONDS;
const MAX_TOKEN_EXPIRY_FOR_RENEW = 2 * DAY_IN_SECONDS;

export const tokenShouldBeRenewed = token => {
	const webToken = jwt.verify(token, tokenPrivateKey);
	const currentSeconds = new Date().getTime() / 1000;
	return currentSeconds - webToken.exp < MAX_TOKEN_EXPIRY_FOR_RENEW;
};

export const getUniqueToken = _id => {
	return jwt.sign({ _id }, tokenPrivateKey, {
		expiresIn: TOKEN_VALID_SECONDS,
	});
};

export const getUserFromToken = async token => {
	try {
		jwt.verify(token, tokenPrivateKey);
	} catch (e) {
		await deleteToken(token);
		throw e;
	}

	const tokenObject = await findToken(token);
	if (!tokenObject) {
		throw new Error("token doesn't exist");
	}

	const user = await findUser({ _id: tokenObject.userId });
	if (!user) {
		throw new Error("User for token not found");
	}

	return user;
};
