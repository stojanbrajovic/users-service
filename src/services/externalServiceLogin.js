import loginUser from "./loginUser";
import { insertUser, findUser } from "../mongodb/users";

const externalServiceLogin = async email => {
	const existingUser = await findUser({ email });
	if (existingUser) {
		return loginUser(existingUser);
	}

	const newUser = await insertUser({ email });
	return loginUser(newUser.ops[0]);
};

export default externalServiceLogin;
